#!/bin/bash

set -eo pipefail

SCRIPT_FOLDER="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BAYS_FOLDER=$1
BAY_NAME=$2
BAY_FOLDER=$BAYS_FOLDER/$BAY_NAME

check_args() {
  if [[ -z $BAYS_FOLDER ]] ; then
    echo "Should pass the folder where to put the bay folder config"
    echo "Example: magnum-bay-config /home/user/bays my-kubernetes-bay-001"
    exit 1
  fi
  if [[ -z $BAY_NAME ]] ; then
    echo "Should pass the name of the bay"
    echo "Example: magnum-bay-config /home/user/bays my-swarm-bay-001"
    exit 1
  fi
}

main() {
  check_args

  echo "Trying to configure bay $BAY_NAME"

  if [[ -d $BAY_FOLDER ]]; then
    echo "Folder for that bay already exists. Bay has already been configured"
    exit 1
  fi

  while true ; do
    STATUS=`magnum bay-list | grep $BAY_NAME`
    echo $STATUS

    if [[ $STATUS == *"FAIL"* ]]; then
      notify-send "Creation of $BAY_NAME failed"
      echo "This bay seems to be in an error state"
      exit 1
    fi

    if [[ $STATUS == *"CREATE_COMPLETE"* ]]; then
      notify-send "Creation of $BAY_NAME finished"
      echo "Creation finished"
      break
    fi

    echo "Waiting 20s"
    sleep 20
  done

  mkdir $BAY_FOLDER
  cd $BAY_FOLDER

  ln -s $BAYS_FOLDER/key.pem $BAY_FOLDER/key.pem
  ln -s $BAYS_FOLDER/cert.csr $BAY_FOLDER/cert.csr

  magnum ca-sign --bay $BAY_NAME --csr $BAY_FOLDER/cert.csr > $BAY_FOLDER/cert.pem
  magnum ca-show --bay $BAY_NAME > $BAY_FOLDER/ca.pem

  if [[ $BAY_NAME == *"kubernetes"* ]]; then
    echo "It seems it's a Kubernetes bay, creating a config file"
    cp $SCRIPT_FOLDER/config $BAY_FOLDER/
    BAY_ENDPOINT=`magnum bay-show $BAY_NAME | grep api_address | awk '{print $4}'`
    sed -i 's;BAY_NAME;'"$BAY_NAME"';g' $BAY_FOLDER/config
    sed -i 's;BAY_FOLDER;'"$BAY_FOLDER"';g' $BAY_FOLDER/config
    sed -i 's;BAY_ENDPOINT;'"$BAY_ENDPOINT"';g' $BAY_FOLDER/config
    echo "kubectl --kubeconfig=$BAY_FOLDER/config"
    echo "or"
    echo "alias kubectl=\"kubectl --kubeconfig=$BAY_FOLDER/config\""
  elif [[ $BAY_NAME == *"swarm"* ]] ; then
    echo "It seems it's a Swarm bay, creating an env file"
    cp $SCRIPT_FOLDER/env.sh $BAY_FOLDER/
    BAY_ENDPOINT=`magnum bay-show $BAY_NAME | grep api_address | awk '{print $4}' | sed 's/https/tcp/g'`
    sed -i 's;BAY_FOLDER;'"$BAY_FOLDER"';g' $BAY_FOLDER/env.sh
    sed -i 's;BAY_ENDPOINT;'"$BAY_ENDPOINT"';g' $BAY_FOLDER/env.sh
    echo "source $BAY_FOLDER/env.sh"
  else
    echo "Not detected any COE type, so not putting any file to configure"
  fi

  echo "cd $BAY_FOLDER"
}

main
